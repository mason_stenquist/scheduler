import Calendar from './components/calendar/Calendar';

function App() {
  const weeks = FM_CONFIG.weeks;
  const appointment = FM_CONFIG.appointment;
  const showCapacity = Boolean(FM_CONFIG.showCapacity);

  return (
    <div class="flex justify-center pt-4">
      <Calendar
        weeks={weeks}
        appointment={appointment}
        showCapacity={showCapacity}
      />
    </div>
  );
}

export default App;
