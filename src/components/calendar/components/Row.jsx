import { createEffect, createMemo, For, onMount, Show } from 'solid-js';
import {
  scheduleCapacity,
  selectedDate,
  selectedWeek,
  setSelectedDate,
  setSelectedWeek
} from '../state';
import Day from './Day';

const Row = ({ weekStartDate, isFirst, showCapacity }) => {
  const days = createMemo(() => {
    let daysArray = [];
    let date = weekStartDate;
    const currentWeek = weekStartDate.week();
    //Keep looping while in same week
    while (currentWeek === date.week()) {
      daysArray.push(date);
      date = date.add(1, 'day');
    }
    return daysArray;
  });

  const maxEstimated = createMemo(() => {
    const scheduleArray = Object.values(scheduleCapacity());
    const max = Math.max(...scheduleArray.map((e) => e?.capacity));
    return Math.round(max * 100);
  });

  const week = () => {
    const start = weekStartDate.format('M/D/YYYY');
    const capacity = scheduleCapacity()[start];
    return capacity;
  };

  const isSelectedWeek = () => {
    if (!week() || !selectedWeek()) {
      return false;
    }
    return week().uuid === selectedWeek().uuid && !selectedDate();
  };

  const capacityPercent = () => Math.round((week()?.capacity || 0) * 100);
  const startMonth = weekStartDate.format('MMM');
  const endMonth = weekStartDate.add(7, 'day').format('MMM');
  const isNewMonth = startMonth !== endMonth;
  const monthLabel = isNewMonth || isFirst ? endMonth : null;

  //Interactions
  const handleWeekClick = () => {
    setSelectedDate(null);
    setSelectedWeek(week());
  };

  //STYLES
  const weekButtonClasses = () => ({
    'text-white bg-blue-500 border-blue-500 rounded-none overflow-hidden pl-3 w-12':
      isSelectedWeek(),
    'h-7 mt-1 ml-2 rounded w-10': !isSelectedWeek()
  });

  const graphWidth = createMemo(
    () => (capacityPercent() / maxEstimated()) * 100
  );
  const graphClasses = () => ({
    'bg-gray-300': capacityPercent() < 65,
    'bg-green-300': capacityPercent() >= 65 && capacityPercent() < 85,
    'bg-yellow-300': capacityPercent() >= 85 && capacityPercent() < 100,
    'bg-red-300 text-white': capacityPercent() >= 100
  });

  const dayContainerClasses = () => ({
    'ring ring-blue-500 z-10 shadow-lg rounded overflow-hidden':
      isSelectedWeek()
  });

  return (
    <div>
      <div class="flex items-center">
        <div class="pl-2 w-12">{monthLabel}</div>
        <div class="flex rounded-lg" classList={dayContainerClasses()}>
          <For each={days()}>{(date) => <Day date={date} week={week} />}</For>
          <Show when={showCapacity && week()}>
            <button
              classList={weekButtonClasses()}
              onclick={() => handleWeekClick()}
              class="ml-2 px-1 border text-xs"
            >
              W{weekStartDate.week()}
            </button>
          </Show>
        </div>
        <Show when={showCapacity && week()}>
          <div class="text-sm ml-2 relative transition duration-300  min-w-[130px] rounded-md shadow-inner">
            <div class="flex px-2 py-1 space-x-2">
              <span>
                {week().hoursEstimated ?? 'NA'}/
                {Math.round(week().hoursScheduled ?? 0)} hrs
              </span>
              <span>{capacityPercent()}%</span>
            </div>
            <div
              classList={graphClasses()}
              class="h-full absolute top-0 z-[-1] rounded-md shadow-md"
              style={{
                width: `${graphWidth()}%`
              }}
            ></div>
            <div class="h-full w-full bg-gray-50 absolute top-0 z-[-2] rounded-md shadow-inner"></div>
          </div>
        </Show>
      </div>
    </div>
  );
};

export default Row;
