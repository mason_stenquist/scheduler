import { createMemo } from 'solid-js';
import {
  events,
  getMatchingHoliday,
  selectedDate,
  setSelectedCapacity,
  setSelectedDate,
  setSelectedEvents,
  setSelectedWeek
} from '../state';

const Day = (props) => {
  const date = props.date;
  const month = date.month();

  const dayCapacity = () => {
    if (!props.week()) {
      return 0;
    }
    const dayOfWeek = date.format('ddd').toLowerCase();
    return props.week()[dayOfWeek];
  };

  const handleClick = () => {
    if (selectedDate === date) {
      setSelectedDate(null);
      setSelectedEvents([]);
      setSelectedCapacity(0);
      setSelectedWeek({});
    } else {
      setSelectedDate(date);
      setSelectedEvents(matchingEvents());
      setSelectedCapacity(dayCapacity());
      setSelectedWeek(props.week());
    }
  };

  const matchingEvents = createMemo(() => {
    return events().filter((event) => event.date === date.format('M/D/YYYY'));
  });

  const matchingHoliday = () => {
    return getMatchingHoliday(date);
  };

  const eventCount = () => matchingEvents().length;

  const remainingCapacity = () => {
    if (matchingHoliday()) return 0;
    return dayCapacity() - eventCount();
  };

  const wrapperBgColor = () => ({
    'bg-blue-100': month % 2 === 1
  });

  const dayClass = () => ({
    'bg-opacity-30': dayCapacity() === 0 && eventCount() === 0,
    'ring ring-blue-500 bg-blue-400': selectedDate() === date,
    'bg-red-600': remainingCapacity() <= 0,
    'bg-green-600': remainingCapacity() > 0
  });

  const title = () => {
    const eventList = matchingEvents()
      .map((event) => `${event.client}`)
      .join('\n');
    return `${date.format('dddd - M/D/YYYY')}\n${eventList}`;
  };

  const buttonDisplay = () => {
    return matchingHoliday() && matchingHoliday().emoji
      ? matchingHoliday().emoji
      : date.date();
  };

  return (
    <div class="p-1.5" classList={wrapperBgColor()}>
      <button
        class="w-6 h-6 rounded-lg text-white shadow-md border-gray-300 hover:ring transition duration-100 text-xs flex items-center justify-center"
        classList={dayClass()}
        onClick={handleClick}
        title={title()}
      >
        {buttonDisplay()}
      </button>
    </div>
  );
};

export default Day;
