import dayjs from 'dayjs';
import { Show } from 'solid-js';
import { goToRecord } from '../api/goToRecord';

const WeekEvent = (props) => {
  const type = () => (props.event?.name ? 'HOLIDAY' : 'EVENT');
  const dateDisplay = () => dayjs(props.event.date, 'M/D/YYYY').format('D ddd');
  return (
    <div class="flex text-sm">
      <div class="min-w-[65px]">{dateDisplay()}</div>
      <div class="flex-grow">
        <Show when={type() === 'EVENT'}>
          <p>
            <button
              class="underline hover:text-blue-600 text-left"
              title="Jump to Record"
              onClick={async () =>
                await goToRecord(68, 'RECORD::z___UUID', props.event.uuid)
              }
            >
              {props.event.client}
              <Show when={props.event.craftName}>
                {' '}
                - {props.event.craftName}
              </Show>
            </button>
          </p>
          <Show when={props.event.notes}>
            <p class="bg-yellow-100 border border-yellow-600 rounded p-1 text-xs my-1">
              {props.event.notes}
            </p>
          </Show>
        </Show>
        <Show when={type() === 'HOLIDAY'}>
          <span class="pr-2">{props.event.emoji}</span>
          {props.event.name}
        </Show>
      </div>
    </div>
  );
};
export default WeekEvent;
