const AppointmentInfo = ({ id, craftName, notes }) => {
  return (
    <>
      <p class="text-gray-800">Record: {id}</p>
      <p class="text-gray-800">Craft: {craftName || 'NA'}</p>
      {notes && (
        <p class="bg-yellow-100 border border-yellow-600 rounded p-1 text-xs my-1">
          {notes}
        </p>
      )}
    </>
  );
};

export default AppointmentInfo;
