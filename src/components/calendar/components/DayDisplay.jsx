import AppointmentInfo from './AppointmentInfo';
import { fmScript } from '../utils/fmScript';
import {
  selectedDate,
  setSelectedDate,
  selectedEvents,
  getMatchingHoliday,
  selectedCapacity,
  setSelectedWeek,
  startDate,
  endDate
} from '../state';
import { Show } from 'solid-js';
import CalendarIcon from '../icons/CalendarIcon';
import { loadData } from '../api/loadData';

const DayDisplay = ({ appointment }) => {
  const eventCount = () => selectedEvents().length;

  const matchingHoliday = () => {
    if (!selectedDate()) {
      return;
    }
    return getMatchingHoliday(selectedDate());
  };

  const remainingCapacity = () => {
    if (matchingHoliday()) return 0;
    return selectedCapacity() - eventCount();
  };

  const displayDate = () =>
    selectedDate() ? selectedDate().format('dddd - M/D/YYYY') : 'Select a date';

  const handleClick = async () => {
    try {
      //829 = record_UPDATE_date_app
      const fmDate = selectedDate().format('M/D/YYYY');
      await fmScript(829, { date: fmDate });
    } catch (e) {
      alert(e);
    }
  };

  const handleCalendarClick = async () => {
    try {
      const params = matchingHoliday()
        ? { holidayID: matchingHoliday().uuid }
        : { date: selectedDate().format('M/D/YYYY') };
      //842 = holiday_CONFIG_open
      await fmScript(842, params);
      await loadData(startDate(), endDate(), false);
    } catch (e) {
      console.error(e);
    }
  };

  const scheduleButtonText = () => {
    return remainingCapacity() <= 0
      ? 'Scheduling disabled for this day'
      : `Book Appointment for ${appointment.client}`;
  };

  return (
    <div class="top-0 absolute">
      <div class=" border w-96 rounded-lg shadow-xl bg-white">
        <div class="mb-2 p-4 border-b">
          <div class="flex items-center justify-between">
            <h1 class="font-bold text-lg">{displayDate()}</h1>
            <button
              class="w-6 h-6 rounded-full bg-gray-100 flex items-center justify-center shadow"
              onClick={() => {
                setSelectedDate(null);
                setSelectedWeek(null);
              }}
            >
              &times;
            </button>
          </div>
          <Show when={matchingHoliday()}>
            <div>
              <p class="text-lg py-1">
                {matchingHoliday().name} {matchingHoliday().emoji}
              </p>
              <p class="text-sm">{matchingHoliday().notes}</p>
            </div>
          </Show>
        </div>

        {eventCount() === 0 && (
          <div class="p-4 text-center font-thin text-xl text-gray-500">
            No appointments on this date yet
          </div>
        )}

        <div class="px-4 divide-y pb-2">
          <For each={selectedEvents()}>
            {(event) => (
              <div class="text-sm py-2">
                <p class="font-bold text-base pb-1">
                  {event.client} {event.time && ` at ${event.time}`}
                </p>
                <AppointmentInfo {...event} />
              </div>
            )}
          </For>
        </div>

        <Show when={appointment}>
          <div class="p-4 border-t">
            <button
              onClick={handleClick}
              disabled={remainingCapacity() <= 0}
              class="disabled:opacity-60 w-full shadow hover:shadow-md rounded py-2 bg-green-700 text-white hover:bg-green-600 transition duration-200"
            >
              {scheduleButtonText()}
            </button>
            <div class="pt-2 text-sm">
              <AppointmentInfo {...appointment} />
            </div>
          </div>
        </Show>

        <div class="px-4 py-2 text-xs text-gray-700 flex justify-evenly border-t items-center">
          <div class="text-center">
            <div class="font-bold text-base">
              {selectedEvents().length}/{selectedCapacity()}
            </div>
            <div class="text-gray-500">Capacity</div>
          </div>
          <div class="text-center">
            <div class="font-bold text-base">{remainingCapacity()}</div>
            <div class="text-gray-500">Remaining Spots</div>
          </div>
          <button
            onClick={() => handleCalendarClick()}
            class="text-center hover:underline hover:shadow-inner hover:ring-1 ring-gray-200 transition duration-200 p-2 rounded"
          >
            <CalendarIcon class="w-6 mx-auto" />
            <div class="text-gray-500 pt-0.5">
              {matchingHoliday() ? 'Edit Holiday' : 'Add Holiday'}
            </div>
          </button>
        </div>
      </div>
    </div>
  );
};

export default DayDisplay;
