import { createEffect, For } from 'solid-js';
import SettingsIcon from '../icons/SettingsIcon';
import {
  endDate,
  events,
  holidays,
  selectedDate,
  selectedWeek,
  setSelectedDate,
  setSelectedWeek,
  startDate
} from '../state';
import Gauge from './Gauge';

import dayjs from 'dayjs';

//Setup Dayjs
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isBetween from 'dayjs/plugin/isBetween';
import WeekEvent from './WeekEvent';
import { fmScript } from '../utils/fmScript';
import TasksIcon from '../icons/TasksIcon';
import ScheduleIcon from '../icons/ScheduleIcon';
import { loadData } from '../api/loadData';
dayjs.extend(customParseFormat);
dayjs.extend(isBetween);

const format = (value) => `${value}%`;

const WeekDisplay = () => {
  const displayDate = () =>
    selectedWeek()?.dateStart + ' - ' + selectedWeek().dateEnd;

  const weekEvents = () => {
    const start = dayjs(selectedWeek()?.dateStart, 'M/D/YYYY');
    const end = dayjs(selectedWeek()?.dateEnd, 'M/D/YYYY');
    //Get record events
    const currentEvents = events().filter((event) =>
      dayjs(event.date, 'M/D/YYYY').isBetween(start, end, null, '[]')
    );
    //get holidays
    const currentHolidays = holidays().filter((holiday) =>
      dayjs(holiday.date, 'M/D/YYYY').isBetween(start, end, null, '[]')
    );
    //combine and sort
    const combined = [...currentEvents, ...currentHolidays];
    const sorted = combined.sort((a, b) =>
      dayjs(a.date, 'M/D/YYYY').diff(dayjs(b.date, 'M/D/YYYY'))
    );
    return sorted;
  };

  const openCapacity = async () => {
    try {
      await fmScript(834, { weekID: selectedWeek().uuid });
      await loadData(startDate(), endDate(), false);
    } catch (e) {
      console.error(e);
    }
  };

  const viewTasks = async () => {
    const params = {
      weekUUID: selectedWeek().uuid,
      divisionUUID: selectedWeek().divisionUUID,
      weekNumber: selectedWeek().weekNumber
    };
    fmScript('278', params);
  };

  const gaugeValue = () =>
    !selectedWeek() ? 0 : Math.round(selectedWeek().capacity * 100);

  const arcColor = () => {
    if (gaugeValue() >= 100) return '#dc2626';
    if (gaugeValue() >= 85) return '#dcb537';
    if (gaugeValue() >= 65) return '#21b57a';
    return 'gray';
  };

  return (
    <div class="top-0 absolute">
      <div class=" border w-96 rounded-lg shadow-xl bg-white ">
        <div class="mb-2 p-4 border-b">
          <div class="flex items-center justify-between">
            <h1 class="font-bold text-lg">{displayDate()}</h1>
            <button
              class="w-6 h-6 rounded-full bg-gray-100 flex items-center justify-center shadow"
              onClick={() => setSelectedWeek(null)}
            >
              &times;
            </button>
          </div>
        </div>
        <div class="p-4 flex justify-center">
          <Gauge
            value={gaugeValue()}
            width={200}
            formatLabel={format}
            formatValue={format}
            valueColor={arcColor()}
            arcColor={arcColor()}
            max={300}
          />
        </div>

        <Show when={selectedWeek().notes}>
          <p class="px-4 pb-4 text-red-600">Notes: {selectedWeek().notes}</p>
        </Show>

        <div class="border-t p-4 space-y-2">
          <p class="font-bold">Schedule</p>
          <For each={weekEvents()}>
            {(event) => <WeekEvent event={event} />}
          </For>
        </div>

        <div class="px-4 py-2 text-xs text-gray-700 flex justify-evenly border-t items-center">
          <div class="text-center">
            <div class="font-bold text-base" style={{ color: arcColor() }}>
              {selectedWeek().hoursEstimated}/
              {Math.round(selectedWeek().hoursScheduled)}
            </div>
            <div class="text-gray-500">Est vs Scheduled</div>
          </div>
          <button
            onClick={openCapacity}
            class="text-center hover:underline hover:shadow-inner hover:ring-1 ring-gray-200 transition duration-200 p-2 rounded"
          >
            <ScheduleIcon class="w-6 mx-auto" />
            <div class="text-gray-500 pt-0.5">Edit Capacity</div>
          </button>
          <button
            onclick={() => viewTasks()}
            class="text-center hover:underline hover:shadow-inner hover:ring-1 ring-gray-200 transition duration-200 p-2 rounded"
          >
            <TasksIcon class="w-6 mx-auto" />
            <div class="text-gray-500 pt-0.5">View Tasks</div>
          </button>
        </div>
      </div>
    </div>
  );
};

export default WeekDisplay;
