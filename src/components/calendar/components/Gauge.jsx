import { mergeProps } from 'solid-js';

const HALF = 0.5;

const mapValue = (value, fromStart, fromEnd, toStart, toEnd) => {
  const fromDiff = fromEnd - fromStart;
  const toDiff = toEnd - toStart;
  return toStart + (toDiff * (value - fromStart)) / fromDiff;
};

const Gauge = (props) => {
  const merged = mergeProps(
    {
      labelFontWeight: 600,
      valueFontWeight: 300,
      labelColor: '#555',
      valueColor: '#333',
      arcColor: '#f00',
      arcBackgroundColor: '#EEE',
      arcWidth: 16,
      labelFontSize: 10,
      labelMargin: 15,
      valueFontSize: 22,
      min: 0,
      max: 100,
      padding: 10
    },
    props
  );

  const arcHeight = merged.width * HALF;
  const arcOffset = merged.arcWidth * HALF + merged.padding;

  // tslint:disable-next-line:max-line-length ter-max-len
  const d = `M ${arcOffset} ${arcHeight} A ${arcHeight - arcOffset} ${
    arcHeight - arcOffset
  } 0 0 1 ${merged.width - arcOffset} ${arcHeight}`;
  // Circumference of the arc
  const circ = Math.PI * (arcHeight - arcOffset);
  const dasharray = () =>
    `${circ * mapValue(merged.value, merged.min, merged.max, 0, 1)} ${circ}`;

  return (
    <svg
      width={merged.width}
      height={
        arcHeight + merged.labelMargin + merged.labelFontSize + merged.padding
      }
    >
      <path
        fill="none"
        stroke={merged.arcBackgroundColor}
        stroke-width={merged.arcWidth}
        d={d}
        stroke-linecap="round"
      />
      <path
        fill="none"
        stroke-dasharray={dasharray()}
        stroke={merged.arcColor}
        stroke-width={merged.arcWidth}
        d={d}
        stroke-linecap="round"
      />
      <text
        x={merged.width * HALF}
        y={
          merged.padding +
          merged.arcWidth +
          (arcHeight - merged.padding - merged.arcWidth) * HALF +
          merged.valueFontSize * HALF
        }
        font-size={merged.valueFontSize}
        text-anchor="middle"
        fill={merged.valueColor}
        font-weight={merged.valueFontWeight}
      >
        {typeof merged.formatValue === 'function'
          ? merged.formatValue(merged.value)
          : merged.value}
      </text>

      <text
        x={arcOffset}
        y={arcHeight + merged.labelFontSize + merged.labelMargin}
        font-size={merged.labelFontSize}
        text-anchor="middle"
        fill={merged.labelColor}
        font-weight={merged.labelFontWeight}
      >
        {typeof merged.formatLabel === 'function'
          ? merged.formatLabel(merged.min)
          : merged.min}
      </text>

      <text
        x={merged.width - arcOffset}
        y={arcHeight + merged.labelFontSize + merged.labelMargin}
        font-size={merged.labelFontSize}
        text-anchor="middle"
        fill={merged.labelColor}
        font-weight={merged.labelFontWeight}
      >
        {typeof merged.formatLabel === 'function'
          ? merged.formatLabel(merged.max)
          : merged.max}
      </text>
    </svg>
  );
};

export default Gauge;
