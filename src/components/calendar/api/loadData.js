import dayjs from 'dayjs';
import {
  selectedWeek,
  setEvents,
  setHolidays,
  setScheduleCapacity,
  setSelectedWeek
} from '../state';
import { getCapacity, getEvents, getHolidays } from './getEvents';

export const loadData = async (dateStart, dateEnd, useCache = true) => {
  const [capacity, holidays, events] = await Promise.all([
    getCapacity(dateStart, dateEnd, useCache),
    getHolidays(dateStart, dateEnd, useCache),
    getEvents(dateStart, dateEnd, useCache)
  ]);
  setScheduleCapacity(capacity);
  setHolidays(holidays);
  setEvents(events);

  if (window.FM_CONFIG.showCapacity) {
    const start = selectedWeek()?.dateStart
      ? selectedWeek()?.dateStart
      : dayjs().day(0).format('M/D/YYYY');
    const week = capacity[start];
    setSelectedWeek(week);
  }
};
