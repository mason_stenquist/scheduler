import { fmScript } from '../utils/fmScript';

export const goToRecord = async (layoutID, field, value) => {
  try {
    await fmScript('fm-bridge-goto', { layoutID, field, value });
  } catch (e) {
    alert(e);
  }
};
