import { fmScript } from '../utils/fmScript';
import { fmFind } from './fmFind';

export const getEvents = async (startDate, endDate, useCache) => {
  const fmStartDate = startDate.format('M/D/YYYY');
  const fmEndDate = endDate.format('M/D/YYYY');
  const layout = 'Record-Schedule';
  const query = {
    limit: 1000,
    query: [
      {
        date_app: `${fmStartDate}...${fmEndDate}`
      }
    ]
  };

  try {
    const results = await fmFind(layout, query, useCache, 60);
    console.log(results);
    const records = results.response.data.map((record) =>
      JSON.parse(record.fieldData.zctScheduleJSON)
    );
    return records;
  } catch (e) {
    console.log(e);
  }
};

export const getHolidays = async (startDate, endDate, useCache) => {
  const fmStartDate = startDate.format('M/D/YYYY');
  const fmEndDate = endDate.format('M/D/YYYY');
  const layout = 'ScheduleEvent-JSON';
  const query = {
    limit: 1000,
    query: [
      {
        date: `${fmStartDate}...${fmEndDate}`
      }
    ]
  };

  try {
    const results = await fmFind(layout, query, useCache, 60 * 60 * 7);
    const records = results.response.data.map((record) =>
      JSON.parse(record.fieldData.zctEventJSON)
    );
    return records;
  } catch (e) {
    console.log(e);
  }
};

export const getCapacity = async (startDate, endDate, useCache) => {
  const fmStartDate = startDate.format('M/D/YYYY');
  const fmEndDate = endDate.format('M/D/YYYY');
  const layout = 'Week-JSON';
  const query = {
    limit: 1000,
    query: [
      {
        Date_Start: `${fmStartDate}...${fmEndDate}`
      }
    ]
  };

  try {
    const results = await fmFind(layout, query, useCache, 60 * 5);
    const capacity = {};
    results.response.data.forEach((record) => {
      const data = JSON.parse(record.fieldData.zctWeekJSON);
      capacity[data.dateStart] = data;
    });
    return capacity;
  } catch (e) {
    console.log(e);
  }
};
