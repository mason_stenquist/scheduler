import { createSignal } from 'solid-js';

export const [startDate, setStartDate] = createSignal(null);
export const [endDate, setEndDate] = createSignal(null);
export const [events, setEvents] = createSignal([]);
export const [holidays, setHolidays] = createSignal([]);
export const [scheduleCapacity, setScheduleCapacity] = createSignal([]);
export const [selectedCapacity, setSelectedCapacity] = createSignal(0);
export const [selectedWeek, setSelectedWeek] = createSignal({});
export const [selectedDate, setSelectedDate] = createSignal(null);
export const [selectedEvents, setSelectedEvents] = createSignal([]);

export const getMatchingHoliday = (date) => {
  return holidays().find((event) => event.date === date.format('M/D/YYYY'));
};
