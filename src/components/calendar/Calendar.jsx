import { createEffect, createMemo, For, onMount, Show } from 'solid-js';
import dayjs from 'dayjs';
import weekOfYear from 'dayjs/plugin/weekOfYear';
dayjs.extend(weekOfYear);
import Row from './components/Row';
import DayDisplay from './components/DayDisplay';
import {
  startDate,
  endDate,
  setStartDate,
  setEndDate,
  selectedWeek,
  scheduleCapacity,
  setSelectedWeek,
  selectedDate
} from './state';
import { loadData } from './api/loadData';
import WeekDisplay from './components/WeekDisplay';

const Calendar = ({ weeks, appointment, showCapacity }) => {
  onMount(async () => {
    const date = dayjs().day(0);
    const dateEnd = date.add(weeks, 'week');
    setStartDate(date);
    setEndDate(dateEnd);
    await loadData(date, dateEnd);
  });

  // createEffect(() => {
  //   if (showCapacity) {
  //     const start = dayjs().day(0).format('M/D/YYYY');
  //     const week = scheduleCapacity()[start];
  //     setSelectedWeek(week);
  //   }
  // });

  const rows = createMemo(() => {
    let date = startDate();
    let rowArray = [];
    while (date && date <= endDate()) {
      rowArray.push(date);
      date = dayjs(date).add(1, 'week');
    }
    return rowArray;
  });

  return (
    <div class="flex space-x-4">
      <div>
        <div class="flex mb-1 pl-12">
          <div class="w-9 text-center font-bold">S</div>
          <div class="w-9 text-center font-bold">M</div>
          <div class="w-9 text-center font-bold">T</div>
          <div class="w-9 text-center font-bold">W</div>
          <div class="w-9 text-center font-bold">T</div>
          <div class="w-9 text-center font-bold">F</div>
          <div class="w-9 text-center font-bold">S</div>
        </div>
        <For each={rows()}>
          {(date, index) => (
            <Row
              weekStartDate={date}
              isFirst={index() === 0}
              showCapacity={showCapacity}
            />
          )}
        </For>
      </div>
      <div className="relative w-96">
        <Show when={selectedDate()}>
          <DayDisplay appointment={appointment} />
        </Show>
        <Show when={selectedWeek() && selectedWeek()?.uuid && !selectedDate()}>
          <WeekDisplay />
        </Show>
      </div>
    </div>
  );
};

export default Calendar;
